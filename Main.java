import java.util.Scanner;
import java.util.Random;

public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        functionality1(scanner);
        functionality2(scanner);
        functionality3();
    }

    public static void functionality1(Scanner scanner)
    {
        System.out.println("ФУНКЦИОНАЛНОСТ 1");
        System.out.println("Моля, отговорете на следните задължителни въпроси:");

        String[] mandatoryQuestions =
                {
                        "Колко тигана има в кухнята?",
                        "Колко тенджери има в кухнята?",
                        "Колко сантиметра е широка кухнята?",
                        "Колко сантиметра е дълга кухнята?",
                        "Колко сантиметра е висока кухнята?",
                        "Как се казва главният готвач?",
                        "Колко готвачи работят в кухнята?",
                        "Колко сервитьори работят в ресторанта?",
                        "Как се казва отговорника на сосовете?",
                        "Каква е минималната температура в кухнята?",
                        "Каква е максималната температура в кухнята?",
                        "Колко парички имате в банковата си сметка?",
                        "Колко парички е дневният оборот на ресторанта ви?",
                        "В колко часа отваряте?",
                        "В колко часа затваряте?"
                };

        for (String question : mandatoryQuestions)
        {
            System.out.print(question + ": ");
            String answer = scanner.nextLine();

        }

        System.out.println("Желаете ли да отговаряте на опционални въпроси? (Y/N)");
        String optionalResponse = scanner.nextLine().toUpperCase();
        if (optionalResponse.equals("Y"))
        {
            String[] optionalQuestions =
                    {
                            "Разполагате ли с хладилник?",
                            "Разполагате ли с газов котлон?",
                            "Разполагате ли с конвектомат?",
                            "Разполагате ли с електрическа скара?",
                            "Разполагате ли с аспиратор?"
                    };

            for (String question : optionalQuestions)
            {
                System.out.print(question + " (Y/N): ");
                String response = scanner.nextLine().toUpperCase();

            }
        }
    }

    public static void functionality2(Scanner scanner)
    {
        System.out.println("ФУНКЦИОНАЛНОСТ 2");

        System.out.println("Какво ще хапвате?");
        System.out.println("(1) Месо");
        System.out.println("(2) Вегетарианско");
        System.out.println("(3) Десерт");
        String choice = scanner.nextLine();

        switch (choice)
        {
            case "1":
                System.out.println("Избрахте месо.");
                System.out.println("Месото има грамаж, като роботът може да готви само в три разновидности на порциите:");
                System.out.println("(1) Малка порция (450 грама)");
                System.out.println("(2) Средна порция (750 грама)");
                System.out.println("(3) Голяма порция (950 грама)");
                System.out.print("Изберете порция: ");
                String portionChoice = scanner.nextLine();
                System.out.println("Месото има 3 начина на приготвяне:");
                System.out.println("(1) RARE");
                System.out.println("(2) MEDIUM");
                System.out.println("(3) WELL DONE");
                System.out.print("Изберете начин на приготвяне: ");
                String preparationChoice = scanner.nextLine();
                break;
            case "2":
                System.out.println("Избрахте вегетарианско.");
                System.out.println("Какво вид вегетарианско ястие желаете?");
                System.out.println("(1) Вегетарианско");
                System.out.println("(2) Веганско");
                System.out.println("(3) Пескатерианско");
                String vegetarianChoice = scanner.nextLine();

                break;
            case "3":
                System.out.println("Избрахте десерт.");
                System.out.println("Какво вид десерт желаете?");
                System.out.println("(1) Торта");
                System.out.println("(2) Сладолед");
                String dessertChoice = scanner.nextLine();
                if (dessertChoice.equals("1")) {
                    System.out.println("Избрахте торта.");

                }
                else if (dessertChoice.equals("2")) {
                    System.out.println("Избрахте сладолед.");
                    System.out.println("Първо, моля, изберете вкус:");
                    System.out.println("(1) Ягода");
                    System.out.println("(2) Банан");
                    System.out.println("(3) Ванилия");
                    String iceCreamFlavor = scanner.nextLine();

                }
                else
                {
                    System.out.println("Невалиден избор за десерт.");
                }
                break;
            default:
                System.out.println("Невалиден избор.");
        }
    }
    public static void functionality3()
    {
        System.out.println("ФУНКЦИОНАЛНОСТ 3");

        Random random = new Random();
        boolean orderFulfilled = random.nextBoolean(); // Връща true или false с 50/50 шанс не мога да ги напиша всичките проверки

        if (orderFulfilled)
        {
            System.out.println("Вашата поръчка е изпълнена успешно.");
            System.out.println("Благодаря ви, че хапнахте при нас.");
        }
        else
        {
            System.out.println("Ресторантът не може да изпълни вашата поръчка.");
            System.out.println("Започваме процедура по самоунищожение.");
            System.out.println("Благодаря ви, че хапнахте при нас.");
        }
    }
}
